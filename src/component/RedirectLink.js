import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class RedirectLink extends Component {

    render() {

        return(
            <Link 
                 to={this.props.link}>{this.props.text}
            </Link>
        );

    }

}