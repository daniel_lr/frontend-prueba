import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Routes from './routes';


function App() {
  return (
    <div >
    <Router >
    <Switch>
      {Routes.map((route, index) => (
        <RouteWithSubRoutes key={index} {...route} />
      ))}
    </Switch>
  </Router>
  </div>
 );
}

function RouteWithSubRoutes(route) {
  return (
    <Route 
      path={route.path}
      exact={route.exact}
      render={props => <route.component  routes={route.routes} {...props} />}
    />
  );
}

export default App;
