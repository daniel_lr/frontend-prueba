import React from 'react';

export default function Error404() {
    return(
        <div className='central'>
            <h1>Something went wrong - 404</h1>
        </div>
    );
}