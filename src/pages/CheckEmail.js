import React from 'react';
import RedirectLink from '../component/RedirectLink';

import Checkmail from '../assets/img/checkemail.jpg';

export default function CheckEmail() {
    return(
        <div className='central'>
            <img className="checkmail" src={Checkmail} />
            <div className="reset"></div>
        </div>
    );
}