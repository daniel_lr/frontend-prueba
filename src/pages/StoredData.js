import React from 'react';

import RedirectLink from '../component/RedirectLink';

export default function StoreDataBank() {
    return(
        <>
        <div className='central'>
            <h1> Your data has been stored correctly!!!! </h1>
        
        <h2>
        Need store more data ?
            <RedirectLink link='/databank' text=' Go' />
        </h2>
        </div>
        </>
    );
}