import React, { useState } from 'react';

import RedirectLink from '../component/RedirectLink';

import { userApi } from '../api/user';

export default function Register() {

    const [ inputs, setInputs ] = useState({
        name: '', phone: '',
        email: '', password: ''
    });

    const changeForm = e => {
        setInputs({
            ...inputs,
            [e.target.name]: e.target.value
        })
    }

    const sendRedirect = () => {
        window.location.href = '/checkmail';
    }

    const submitHandler = async e => {
        e.preventDefault(); 
        const result = await userApi(inputs, 'user');

        if(result.ok){
            sendRedirect();
        }
    };

        return(
            <div className='central'>
                <h1>Sign up!</h1>
                <form onSubmit={submitHandler} onChange={changeForm} className='form_container'>
                    <p>
                        <label htmlFor='name'>Name: </label>
                        <input id='name' 
                            name='name'
                            type='text'
                            required
                            onChange={ changeForm }
                            placeholder='Your name' 
                            value = { inputs.name } />
                    </p>
                    <p>
                        <label htmlFor='email'>Email: </label>
                        <input id='email' 
                            name='email'
                            required
                            onChange={ changeForm }
                            type='email'
                            placeholder='Your email'
                            value = { inputs.email }  />
                    </p>
                    <p>
                        <label htmlFor='phone'>Phone number: </label>
                        <input id='phone' 
                            name='phone'
                            type='text'
                            required
                            onChange={ changeForm }
                            placeholder='Your telephone number'  
                            value = { inputs.phone }  />
                    </p>
                    <p>
                        <label htmlFor='password'>Password: </label>
                        <input id='password' 
                            name='password'
                            type='password'
                            required
                            onChange={ changeForm }
                            placeholder='Your password'
                            value = { inputs.password }  />
                    </p>
                    <button >Register</button>
                </form>
                
            </div>
        ); 
}