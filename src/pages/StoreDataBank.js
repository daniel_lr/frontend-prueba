import React, { useState } from 'react';

import RedirectLink from '../component/RedirectLink';
import { ACCESS_TOKEN, REFRESH_TOKEN } from '../utils/constants';

import { userApi } from '../api/user';

export default function StoreDataBank() {

    const [ inputs, setInputs ] = useState({
            iban: 'ES6621000418401234567891', billingAddress: 'c/ Noseque 23', nif: '503333503v'
    });

    const changeForm = e => {
        setInputs({
            ...inputs,
            [e.target.name]: e.target.value
        })
    }

    const sendRedirect = () => {
        window.location.href = '/checkmail';
    }

    const submitHandler = async e => {
        e.preventDefault(); 

        const token = localStorage.getItem(ACCESS_TOKEN);
        const result = await userApi(inputs, 'databank?token=' + token);
        if(result.ok){
            sendRedirect();
        }
    };

        return(
            <div className='central'>
                <h2>Bank Data Form</h2>
                <form onSubmit={submitHandler} onChange={changeForm} className='form_container' >
                    <p>
                        <label htmlFor='iban'>IBAN: </label>
                        <input className='register-form__input'
                            id='iban' 
                            name='iban'
                            required
                            onChange={ changeForm }
                            type='text'
                            placeholder='Your iban'
                            value = { inputs.iban }  />
                    </p>
                    <p>
                        <label htmlFor='billingAddress'>Billing Address: </label>
                        <input className='register-form__input'
                            id='billingAddress' 
                            name='billingAddress'
                            onChange={ changeForm }
                            type='text'
                            placeholder='Your billingAddress' 
                            value = { inputs.billingAddress }  />
                    </p>
                    <p>
                        <label htmlFor='nif'>DNI: </label>
                        <input className='register-form__input'
                            id='nif' 
                            name='nif'
                            onChange={ changeForm }
                            type='text'
                            placeholder='Your Dni' 
                            value = { inputs.nif }  />
                    </p>
                    <button> Save </button>
                </form>
            </div>
        );

}
