import React from 'react';

import RedirectLink from '../component/RedirectLink';
import moneyFalling from '../assets/img/money-falling.png';

export default function Home() {
    return(
        <div className='central'>

            <h1>Welcome !!!</h1>

            <h2>To access our services it is necessary to be registered</h2>

            <img className="money" src={moneyFalling} />
            <div className="reset"></div>
        </div>
    );
}