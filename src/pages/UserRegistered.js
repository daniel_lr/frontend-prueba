import React from 'react';
import RedirectLink from '../component/RedirectLink';

import Success from '../assets/img/success.png';

export default function UserRegistered() {
    return(
        <div className='central'>
            <h1>Registration completed successfully</h1>

            <img className="checkmail" src={Success} />
            <div className="reset"></div>
            <h1 className="btn-center">
                    Maybe you need to 
                    <RedirectLink link='/login' text=' Login' />
            </h1>

        </div>
    );
}