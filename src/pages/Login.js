import React, { useState } from 'react';
import RedirectLink from '../component/RedirectLink';
import { ACCESS_TOKEN } from '../utils/constants';

import { userApi } from '../api/user';

import '../assets/css/styles.css';

export default function Login() {

	const [ inputs, setInputs ] = useState({

        email: '', password: ''

    });
    
    const changeForm = e => {
        setInputs({
            ...inputs,
            [e.target.name]: e.target.value
        });
    }

    const sendRedirect = () => {
        window.location.href = '/databank';
    }

     const submitHandler = async e => {
        e.preventDefault(); 
        const result = await userApi(inputs, 'user/login');

        if(result.ok){
            localStorage.setItem(ACCESS_TOKEN, result.token) ;
            sendRedirect();
        }
    };

        return(
            <div className='central'>
                <h1>Log in into your account</h1>
                <form onSubmit={submitHandler} onChange={changeForm} className='form_container' >
                    <p>
                        <label htmlFor='email'>Email: </label>
                        <input 
                            id='email' 
                            name='email'
                            type='email'
                            required
                            onChange={ changeForm }
                            placeholder='Your email'
                            value = { inputs.email }  />
                    </p>
                    <p>
                        <label htmlFor='password'>Password: </label>
                        <input 
                            id='password' 
                            name='password'
                            type='password'
                            required
                            onChange={ changeForm }
                            placeholder='Your password' 
                            value = { inputs.password }  />
                    </p>
                    <button>Login</button>
                </form>
            </div>
        ); 
    
}