import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import Logo from '../assets/img/logo.png';

import RedirectLink from '../component/RedirectLink';

export default class LayoutBasic extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loggedInStatus: 'NOT_LOGGED_IN',
            token: {}
        }
    }
    getYear() {
        return new Date().getFullYear();
    }

render() {
    const { routes } = this.props;

    return(
       
        <div className='container'>

            <div className='header'>
                <img className='izquierda' src={Logo} alt='logo' />
                <aside> Fake Bank </aside>
                <div className='reset'></div>
            </div>
            <div> 
                <aside className='sidebar home'>
                <div className="block_aside">
                <ul>
                    <li><p>
                        <RedirectLink link='/' text='Home   ' />
                        </p>
                    </li>
                    <li><p>
                        <RedirectLink link='/login' text='Login   ' />
                        </p>
                    </li>
                    <li><p>
                        <RedirectLink link='/Register' text='Register' />
                        </p>
                    </li>
                </ul>
                </div>
                </aside>
                <div className='content'>
                    <LoadRoutes routes={routes} />
                </div>
                <div className='footer'>
                    &copy; { this.getYear() } Daniel Lopesino
                </div>
            </div>
        </div>
    );
}
}

function LoadRoutes({routes}) { 
    return(
        <Switch>
            {routes.map((route, index) => (
            <Route
                key = { index }
                path = { route.path }
                exact = { route.exact }
                component = { route.component } />
            ))}
        </Switch>
    );
}