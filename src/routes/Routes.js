
import LayoutBasic from '../layouts/LayoutBasic';

import Register from '../pages/Register';
import Login from '../pages/Login';
import Home from '../pages/Home';
import Error404 from '../pages/Error404';
import toStoreData from '../pages/StoreDataBank';
import Registered from '../pages/UserRegistered';
import CheckEmail from '../pages/CheckEmail';
import StoredData from '../pages/StoredData';

const routes = [
    {
        path: '/', component: LayoutBasic, exact: false, 
        routes: [
            {
                path: '/', component: Home, exact: true
            },
            {
                path: '/login', component: Login, exact: true
            },
            {
                path: '/register', component: Register, exact: true
            },
            {
                path: '/databank', component: toStoreData, exact: true
            },
            {
                path: '/registered', component: Registered, exact: true
            },
            {
                path: '/checkmail', component: CheckEmail, exact: true
            },
            {
                path: '/success', component: Registered, exact: true 
            },
            {
                path: '/stored', component: StoredData, exact: true 
            },
            {
               component: Error404
            }
        ]
    }
];

export default routes;
