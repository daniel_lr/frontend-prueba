import { serverUrl } from './config';

export function userApi(data, path) {
    const url = `${serverUrl}/${path}`;
    const params = {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    };
return fetch( url, params ).then( response => {
        return response.json();
    }).then( result => {
         return result 
    }).catch(err => {
        return err.message;
    });

}
